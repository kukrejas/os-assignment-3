﻿using System;
using System.Threading;

/* Author: Sumit Kukreja */

namespace OSAssignment3
{
    class Program
    {

        /***********************************
         *         Global Variables        *
         ***********************************/
        public static int bufferMax = 10; //Set the max size of the buffer
        public static int[] buffer = new int[0]; //Initialize the buffer globally so all threads can access it
        public static int counter = 0; //Counts the number of threads created so the program can end at some point
        private static System.Object criticalSection = new System.Object(); //Defines a lock object


        /***********************************
         *      Main Program Execution     *
         ***********************************/
        static void Main(string[] args)
        {
            Console.WriteLine("Starting Application...");
            while (counter < 100) //Ends program after 100 threads
            {
                Thread t = new Thread(() => Execute(new Random().Next(1,3))); //Creates a thread and randomly picks whether it will be a producer or consumer
                t.Start();
                Thread.Sleep(400); //Slows down executions so output can be followed in console window
            }
        }

        /***********************************
         *         Thread Definition       *
         ***********************************/
        public static void Execute(int command) //The method executed by each thread
        {
            lock (criticalSection) //Defines the critical section of the method that is locked so it is not executed until other threads have finished
            {
                switch (command) //Read the input that determines if the thread is a consumer or producer
                {
                    case 1: //Call Producer Logic
                        buffer = Produce(buffer);
                        break;
                    case 2: //Call Consumer Logic
                        buffer = Consume(buffer);
                        break;
                }

                counter++; //Track number of threads created
            }
        }

        /***********************************
         *          Producer Logic         *
         ***********************************/

        public static int[] Produce(int[] buffer)
        {
            if (buffer.Length < 10) //Check if the buffer is overloaded
            {
                int[] newBuffer = new int[buffer.Length + 1]; //Create a new buffer larger than the original so it can hold an additional value
                if (buffer.Length == 0)
                {
                    newBuffer[0] = 1; //If the buffer was originally empty then simply make the first value 1
                }
                else
                {
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        newBuffer[i] = buffer[i]; //Transfer values from original smaller buffer to new one
                    }
                    newBuffer[buffer.Length] = newBuffer[buffer.Length-1] + 1; //Set the last newly added value to be the previous value + 1
                }
                Console.WriteLine("A producer made a cookie with " + newBuffer[newBuffer.Length - 1] + " chocolate chip(s)...");
                return newBuffer;
            }
            else //If buffer is overloaded then don't do anything
            {
                Console.WriteLine("Oh no! There are too many cookies.");
                return buffer;
            }
        }

        /***********************************
         *          Consumer Logic         *
         ***********************************/

        public static int[] Consume(int[] buffer)
        {
            if (buffer.Length > 0) //Check if the buffer is empty
            {
                int[] newBuffer = new int[buffer.Length - 1]; //Create a new buffer with a smaller size since we're removing a value
                for (int i = 0; i < (buffer.Length-1); i++) //Iterate through original buffer leaving out the last value
                {
                    newBuffer[i] = buffer[i]; //Transfer values from original larger buffer to the new smaller one
                }
                Console.WriteLine("A consumer ate the cookie with " + buffer[buffer.Length - 1] + " chocolate chip(s)...");
                return newBuffer;
            }
            else //If buffer is empty then don't do anything
            {
                Console.WriteLine("Oh no! A consumer tried eating a cookie but there were none left.");
                return buffer;
            }
        }
    }
}
